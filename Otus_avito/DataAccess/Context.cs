﻿using Otus_avito.Core.Entities;
using Microsoft.EntityFrameworkCore;


namespace Otus_avito.DataAccess
{
    public class Context: DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Lot> Lots { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public Context()
        {
           // Database.EnsureDeleted();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=otus_avito;Username=postgres;Password=Ctr7724127703");
        }
    }
}
