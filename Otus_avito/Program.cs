﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus_avito.Core.Entities;
using Otus_avito.Core.Services;
using Otus_avito.DataAccess;
using Otus_avito.UI;

namespace Otus_avito
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Context db = new Context())
            {
                //Создаем менеджер и добавляем данные
                AvitoManager manager = new AvitoManager(db);
                if (!db.Customers.Any())
                {
                    //Данные для начального заполнения таблиц
                    Customer masha = new Customer { Name = "Maша", Phone = "+79112343122" };
                    Customer misha = new Customer { Name = "Mиша", Phone = "+79112346442" };
                    Customer ourAll = new Customer { Name = "Александр Пушкин", Phone = "+79212944542" };
                    Customer master = new Customer { Name = "Гражданин Начальник", Phone = "+79521290098" };
                    Customer margo = new Customer { Name = "Маргарита", Phone = "+79521290099" };

                    List<Lot> lots = new List<Lot>();
                    lots.Add(new Lot { Name = "зимняя куртка Kerry 92р", Price = 2500.0, Description = "Отличная теплая куртка на мальчика 92 р, мало б/у, состояние новой", Seller = masha });
                    lots.Add(new Lot { Name = "3-комн. кв, 78м2", Price = 11000000.0, Description = "Продается прекрасная видовая квартира в теплом кирпичном доме, один взрослый собственник, прямая продажа, рассмотрим ипотеку", Seller = misha });
                    lots.Add(new Lot { Name = "участок 9 соток", Price = 900000.0, Description = "Продается участок ИЖС, ровный, прямоугольной формы, электричество по границе участка, газ в 2023г. В непосредственной близости от участка грибной лес, в 20 минутах езды озеро. Разумный торг", Seller = misha });
                    lots.Add(new Lot { Name = "коньки хоккейные профессиональные", Price = 17000.0, Description = "Коньки Ribcore 70K . В эксплуатации пол сезона. Состояние хорошее.", Seller = masha });
                    lots.Add(new Lot { Name = "рукопись коллекционная", Price = 17000.0, Description = "Рукопись известного романа известного писателя. Не горит", Seller = margo });


                    manager.AddCustomers(masha, misha, ourAll, master, margo);
                    manager.AddLots(lots);
                }
                Presenter presenter = new Presenter(manager);
                presenter.MainCycle();
            }
        }
    }
}
