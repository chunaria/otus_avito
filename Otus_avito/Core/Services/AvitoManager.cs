﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Otus_avito.DataAccess;
using Microsoft.EntityFrameworkCore;
using Otus_avito.Core.Entities;

namespace Otus_avito.Core.Services
{
    public class AvitoManager
    {
        private Context db;

        public AvitoManager(Context context)
        {
            db = context;
            GetCustomers();
            GetLots();
            GetPurchases();
        }

        public void AddLot(Lot item)
        {
            db.Lots.Add(item);
            db.SaveChanges();
        }

        public void AddLots(IEnumerable<Lot> items)
        {
            db.Lots.AddRange(items);
            db.SaveChanges();
        }

        public void AddLots(params Lot[] items)
        {
            db.Lots.AddRange(items);
            db.SaveChanges();
        }
        public void AddCustomer(Customer item)
        {
            db.Customers.Add(item);
            db.SaveChanges();
        }

        public void AddCustomers(params Customer[] items)
        {
            db.Customers.AddRange(items);
            db.SaveChanges();
        }

        public void AddCustomers(IEnumerable<Customer> items)
        {
            db.Customers.AddRange(items);
            db.SaveChanges();
        }

        private void AddPurchase(Purchase item)
        {
            db.Purchases.Add(item);
            db.SaveChanges();
        }

        public bool MakePurchase(Customer buyer, Lot lot)
        {
            Purchase purchase = new Purchase {Buyer = buyer, Date = DateTime.Now, Lot = lot};
            if (lot.SoldOut)
            {
                return false;
            }
            lot.SoldOut = true;
            AddPurchase(purchase);
            return true;
        }
        public List<Customer> GetCustomers(Expression<Func<Customer, bool>> filter = null)
        {
            return filter != null ? db.Customers.Where(filter).ToList() : db.Customers.ToList();
        }

        public List<Lot> GetLots(Expression<Func<Lot, bool>> filter = null)
        {
            return filter != null ? db.Lots.Where(filter).ToList() : db.Lots.ToList();
        }

        public List<Purchase> GetPurchases(Expression<Func<Purchase, bool>> filter = null)
        {
            return filter != null ? db.Purchases.Where(filter).ToList() : db.Purchases.ToList();
        }
    }
}
