﻿using System;

namespace Otus_avito.Core.Entities
{
    public class Purchase
    {
        public int id { get; set; }

        public int BuyerId { get; set; }
        public Customer Buyer { get; set; }

        public DateTime Date { get; set; }

        public int  LotId { get; set; }
        public Lot Lot { get; set; }
    }
}
