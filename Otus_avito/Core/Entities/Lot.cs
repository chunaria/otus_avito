﻿
using System.Collections.Generic;

namespace Otus_avito.Core.Entities
{
    public class Lot
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public  double Price { get; set; }
        public  bool SoldOut { get; set; }

        public int SellerId { get; set; }
        public Customer Seller { get; set; }

        public List<Purchase> Purchases { get; set; }
    }
}
