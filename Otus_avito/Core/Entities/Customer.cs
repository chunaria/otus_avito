﻿using System.Collections.Generic;

namespace Otus_avito.Core.Entities
{
    public class Customer
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public int Rating { get; set; }

        public List<Lot> Lots { get; set; }
        public List<Purchase> Purchases { get; set; }
    }
}
