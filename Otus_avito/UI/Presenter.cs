﻿using System;
using System.Collections.Generic;
using Otus_avito.Core.Entities;
using Otus_avito.Core.Services;

namespace Otus_avito.UI
{
    public class Presenter
    {
        private AvitoManager manager;

        public Presenter(AvitoManager manager)
        {
            this.manager = manager;
        }

        public void PrintMenu()
        {
            Console.WriteLine("Выберите действие");
            Console.WriteLine("1 - Посмотреть предложения");
            Console.WriteLine("2 - Посмотреть список пользователей");
            Console.WriteLine("3 - Посмотреть статистику покупок");
            Console.WriteLine("4 - Зарегистрировать нового пользователя");
            Console.WriteLine("5 - Добавить предложение");
            Console.WriteLine("6 - Купить");
            Console.WriteLine("7 - Выход");
        }

        //задание 3.1
        public void PrintCustomers()
        {
            int i = 0;
            foreach (var customer in manager.GetCustomers())
            {
                Console.WriteLine($"{++i}. {customer.Name} {customer.Phone}");
            }
        }

        //задание 3.2
        public void PrintLots()
        {
            int i = 0;
            foreach (var lot in manager.GetLots())
            {
                string status = lot.SoldOut ? " ПРОДАНО" : string.Empty;
                Console.WriteLine($"{++i}. {lot.Name} {lot.Description} {lot.Price}{status} продает {lot.Seller.Name}");
            }
        }

        //задание 3.3
        public void PrintPurchases()
        {
            int i = 0;
            foreach (var purchase in manager.GetPurchases())
            {
                Console.WriteLine($"{++i}. {purchase.Lot.Name} {purchase.Lot.Price} куплен {purchase.Buyer.Name} у {purchase.Lot.Seller.Name} {purchase.Date}");
            }
        }

        public void MainCycle()
        {
            while (true)
            {
                PrintMenu();
                int command = GetInput(7, "Введите корректный номер команды");
                switch (command)
                {
                    case 1:
                        PrintLots();
                        Console.ReadLine();
                        break;
                    case 2:
                        PrintCustomers();
                        Console.ReadLine();
                        break;
                    case 3:
                        PrintPurchases();
                        Console.ReadLine();
                        break;
                    case 4:
                        AddCustomer();
                        PrintCustomers();
                        Console.ReadLine();
                        break;
                    case 5:
                        AddLot();
                        PrintLots();
                        Console.ReadLine();
                        break;
                    case 6:
                        AddPurchase();
                        PrintPurchases();
                        Console.ReadLine();
                        break;
                    case 7:
                        return;
                }
            }
        }

        private int GetInput(int limit, string errorPhrase)
        {
            string input = Console.ReadLine();
            int result;
            while (!int.TryParse(input, out result) || result <= 0 || result > limit)
            {
                Console.WriteLine(errorPhrase);
                input = Console.ReadLine();
            }

            return result;
        }

        private double GetInput(double limit, string errorPhrase)
        {
            string input = Console.ReadLine();
            double result;
            while (!double.TryParse(input, out result) || result <= 0 || result > limit)
            {
                Console.WriteLine(errorPhrase);
                input = Console.ReadLine();
            }

            return result;
        }

        //задание 4.1
        private void AddCustomer()
        {
            Console.WriteLine("Введите имя пользователя");
            string name = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(name))
            {
                Console.WriteLine("Введите непустое имя пользователя");
            }
            Console.WriteLine("Введите номер телефона пользователя");
            string phone = Console.ReadLine();
            manager.AddCustomer(new Customer { Name = name, Phone = phone });
        }

        //задание 4.2
        private void AddLot()
        {
            List<Customer> customers = manager.GetCustomers();
            PrintCustomers();

            Console.WriteLine("Для добавления лота введите ваш порядковый номер участника");
            int number = GetInput(customers.Count, "Введите корректный номер участника") - 1;
            Customer seller = customers[number];

            Console.WriteLine("Введите название лота");
            string name = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(name))
            {
                Console.WriteLine("Введите непустое название лота");
            }

            Console.WriteLine("Введите описание лота");
            string description = Console.ReadLine();

            Console.WriteLine("Введите цену лота");
            double price = GetInput(double.MaxValue, "Введите корректное число для цены");

            manager.AddLot(new Lot { Name = name, Price = price, Description = description, Seller = seller });
        }

        //задание 4.3
        private void AddPurchase()
        {
            List<Customer> customers = manager.GetCustomers();
            List<Lot> lots = manager.GetLots();
            PrintCustomers();

            Console.WriteLine("Для совершения покупки введите ваш порядковый номер участника");
            int buyerNumber = GetInput(customers.Count, "Введите корректный номер участника") - 1;
            Customer buyer = customers[buyerNumber];

            PrintLots();
            Console.WriteLine("Введите порядковый номер лота");
            int lotNumber = GetInput(customers.Count, "Введите корректный номер лота") - 1;
            Lot lot = lots[lotNumber];
            if (!manager.MakePurchase(buyer, lot))
            {
                Console.WriteLine("Этот лот уже продан");
            };
        }
    }
}
